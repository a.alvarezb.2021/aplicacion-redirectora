import random
import socket

myPort = 8080 #Puerto que se va a usar

random_num = str(random.randint(1, 10000)) #Genero un número aleatorio, para la url

random_url = "http://localhost:" + str(myPort) + "/" + random_num #Genero la url aleatoria

redirect_url = ['http://google.com', 'http://wikipedia.org', 'http://microsoft.com', random_url] #Lista de las urls que se van a redirigir

myServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #Creando el socket, de tipo TCP/IP y con el protocolo IPV4, AF_INET indica IPv4, SOCK_STREAM indica TCP
myServer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #Para que el socket se pueda volver a usar, es decir, usar es mismo puerto
myServer.bind(('', myPort)) #se utiliza para vincular un socket a una dirección y número de puerto específicos

myServer.listen(5) #Espera a que se conecten 5 clientesn num max de clientes

last_url = None
current_url = None

try:
    while True:
        print('Waiting for connection...')
        (recvSocket, address) = myServer.accept() #Espera conexiones, las acepta la conexión, y devuelve un nuevo socket para el cliente
        print('HTTP request received from: ', address)
        received = recvSocket.recv(2048) #Recibir datos del cliente. 2048 es el tamaño del buffer. Recibo string de bytes
        received = received.decode('utf-8') #Decodifico lo que me llega, si no me aparece b
        print(received)
        current_url = random.choice(redirect_url) #Se elige una url aleatoria para redireccionar
        response_found = f"HTTP/1.1 302 Found\r\nLocation: {current_url}\r\n\r\n" #Respuesta vaya a redireccionar
        response_not_modified = f"HTTP/1.1 304 Not Modified\r\n\r\n" #Respuesta cuando no haya cambios
        if last_url == current_url:
            recvSocket.send(response_not_modified.encode('utf-8')) #Si se recibe la misma url, se envía la respuesta que no hay cambios
        else:
            recvSocket.send(response_found.encode('utf-8')) #Si no se recibe la misma url, se envía la respuesta que va a redireccionar
            last_url = current_url
        recvSocket.close() #Cerramos el socket

except KeyboardInterrupt:
    print('Closing server...')
    myServer.close() #Cerramos el socket